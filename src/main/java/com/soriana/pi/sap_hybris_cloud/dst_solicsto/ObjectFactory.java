
package com.soriana.pi.sap_hybris_cloud.dst_solicsto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.soriana.pi.sap_hybris_cloud.dst_solicsto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTDSTSolicSTO_QNAME = new QName("http://soriana.com/pi/sap_hybris_cloud/DST_SolicSTO", "MT_DSTSolicSTO");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.soriana.pi.sap_hybris_cloud.dst_solicsto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTDSTSolicSTO }
     * 
     */
    public DTDSTSolicSTO createDTDSTSolicSTO() {
        return new DTDSTSolicSTO();
    }

    /**
     * Create an instance of {@link DTDSTSolicSTO.Articulos }
     * 
     */
    public DTDSTSolicSTO.Articulos createDTDSTSolicSTOArticulos() {
        return new DTDSTSolicSTO.Articulos();
    }

    /**
     * Create an instance of {@link DTDSTSolicSTO.Articulos.Articulo }
     * 
     */
    public DTDSTSolicSTO.Articulos.Articulo createDTDSTSolicSTOArticulosArticulo() {
        return new DTDSTSolicSTO.Articulos.Articulo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTDSTSolicSTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DTDSTSolicSTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://soriana.com/pi/sap_hybris_cloud/DST_SolicSTO", name = "MT_DSTSolicSTO")
    public JAXBElement<DTDSTSolicSTO> createMTDSTSolicSTO(DTDSTSolicSTO value) {
        return new JAXBElement<DTDSTSolicSTO>(_MTDSTSolicSTO_QNAME, DTDSTSolicSTO.class, null, value);
    }

}
