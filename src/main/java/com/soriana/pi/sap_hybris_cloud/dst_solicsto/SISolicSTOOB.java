
package com.soriana.pi.sap_hybris_cloud.dst_solicsto;

import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.3.2
 * Generated source version: 2.2
 * 
 */
@WebService(name = "SI_SolicSTO_OB", targetNamespace = "http://soriana.com/pi/sap_hybris_cloud/DST_SolicSTO")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    ObjectFactory.class
})
public interface SISolicSTOOB {


    /**
     * 
     * @param mtDSTSolicSTO
     */
    @WebMethod(operationName = "SolicSTO", action = "http://sap.com/xi/WebService/soap1.1")
    @Oneway
    public void solicSTO(
        @WebParam(name = "MT_DSTSolicSTO", targetNamespace = "http://soriana.com/pi/sap_hybris_cloud/DST_SolicSTO", partName = "MT_DSTSolicSTO")
        DTDSTSolicSTO mtDSTSolicSTO);

}
