
package com.soriana.pi.sap_hybris_cloud.dst_solicsto;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for DT_DSTSolicSTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DT_DSTSolicSTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Fec_Doc" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="OrdenVenta" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Consignment" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="Articulos"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="Articulo" maxOccurs="unbounded" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="NumPosicion" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *                             &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                             &lt;element name="CantSolic" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_DSTSolicSTO", propOrder = {
    "fecDoc",
    "ordenVenta",
    "consignment",
    "articulos"
})
public class DTDSTSolicSTO {

    @XmlElement(name = "Fec_Doc", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar fecDoc;
    @XmlElement(name = "OrdenVenta", required = true)
    protected String ordenVenta;
    @XmlElement(name = "Consignment", required = true)
    protected String consignment;
    @XmlElement(name = "Articulos", required = true)
    protected DTDSTSolicSTO.Articulos articulos;

    /**
     * Gets the value of the fecDoc property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFecDoc() {
        return fecDoc;
    }

    /**
     * Sets the value of the fecDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFecDoc(XMLGregorianCalendar value) {
        this.fecDoc = value;
    }

    /**
     * Gets the value of the ordenVenta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdenVenta() {
        return ordenVenta;
    }

    /**
     * Sets the value of the ordenVenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdenVenta(String value) {
        this.ordenVenta = value;
    }

    /**
     * Gets the value of the consignment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsignment() {
        return consignment;
    }

    /**
     * Sets the value of the consignment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsignment(String value) {
        this.consignment = value;
    }

    /**
     * Gets the value of the articulos property.
     * 
     * @return
     *     possible object is
     *     {@link DTDSTSolicSTO.Articulos }
     *     
     */
    public DTDSTSolicSTO.Articulos getArticulos() {
        return articulos;
    }

    /**
     * Sets the value of the articulos property.
     * 
     * @param value
     *     allowed object is
     *     {@link DTDSTSolicSTO.Articulos }
     *     
     */
    public void setArticulos(DTDSTSolicSTO.Articulos value) {
        this.articulos = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="Articulo" maxOccurs="unbounded" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="NumPosicion" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
     *                   &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *                   &lt;element name="CantSolic" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "articulo"
    })
    public static class Articulos {

        @XmlElement(name = "Articulo")
        protected List<DTDSTSolicSTO.Articulos.Articulo> articulo;

        /**
         * Gets the value of the articulo property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the articulo property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getArticulo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DTDSTSolicSTO.Articulos.Articulo }
         * 
         * 
         */
        public List<DTDSTSolicSTO.Articulos.Articulo> getArticulo() {
            if (articulo == null) {
                articulo = new ArrayList<DTDSTSolicSTO.Articulos.Articulo>();
            }
            return this.articulo;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="NumPosicion" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
         *         &lt;element name="SKU" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
         *         &lt;element name="CantSolic" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "numPosicion",
            "sku",
            "cantSolic"
        })
        public static class Articulo {

            @XmlElement(name = "NumPosicion", required = true)
            protected BigInteger numPosicion;
            @XmlElement(name = "SKU", required = true)
            protected String sku;
            @XmlElement(name = "CantSolic", required = true)
            protected BigDecimal cantSolic;

            /**
             * Gets the value of the numPosicion property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNumPosicion() {
                return numPosicion;
            }

            /**
             * Sets the value of the numPosicion property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNumPosicion(BigInteger value) {
                this.numPosicion = value;
            }

            /**
             * Gets the value of the sku property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSKU() {
                return sku;
            }

            /**
             * Sets the value of the sku property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSKU(String value) {
                this.sku = value;
            }

            /**
             * Gets the value of the cantSolic property.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getCantSolic() {
                return cantSolic;
            }

            /**
             * Sets the value of the cantSolic property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setCantSolic(BigDecimal value) {
                this.cantSolic = value;
            }

        }

    }

}
