# jaxwsCodeGen

proyecto para generar clases con jaxws a partir de archivos WSDL 

#Pasos
 - Agregar el Wsdl deseado a la carpeta src/main/resources/wsdl
 - en el archivo pom.xml agregar la ruta de tu wsdl y la ruta de generacion de clases
 ```bash
 <sourceDestDir>src/main/java</sourceDestDir>
    <wsdlUrls>
        <wsdl>${project.basedir}/src/main/resources/wsdl/TU-WSDL-AQUI.wsdl</wsdl>
        <!--<wsdlUrl></wsdlUrl> -->
    </wsdlUrls>
  ```
 - si se requiere una url externa utilizar wsdlUrl
 - abrir una cmd y ir a la ruta de tu proyecto, correr el siguiente comando
  ```bash
  mvn jaxws:wsimport
   ```